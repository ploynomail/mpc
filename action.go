package main

import (
	"github.com/txn2/txeh"
)

// ChangeHost change hosts FQDN between at local address and remote address
func ChangeHost(FQDN, localaddr, remoteaddr string, switchMode bool) {
	hosts, err := txeh.NewHostsDefault()
	if err != nil {
		panic(err)
	}
	hosts.RemoveHosts([]string{FQDN})
	if switchMode {
		hosts.AddHost(remoteaddr, FQDN)
	} else {
		hosts.AddHost(localaddr, FQDN)
	}

	hosts.Save()
}
