package main

import (
	"errors"
	"net"
	"strconv"
	"time"
)

func tcpGather(addr string, port int, timeout int64, failure int) (res bool, err error) {
	var result bool
	for i := 0; i < failure; i++ {
		address := net.JoinHostPort(addr, strconv.Itoa(port))
		conn, err := net.DialTimeout("tcp", address, time.Duration(timeout)*time.Second)
		if err != nil {
			result = false
		}
		if conn != nil {
			defer conn.Close()
			result = true
		} else {
			result = false
		}
		time.Sleep(1 * time.Second)
	}
	if result {
		return result, nil
	}
	return false, errors.New("port check faile")
}
