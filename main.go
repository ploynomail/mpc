package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Usage: "Monitor port to modify hosts",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "localaddr",
				Value:       "127.0.0.1",
				Usage:       "local address",
				DefaultText: "127.0.0.1",
			},
			&cli.StringFlag{
				Name:     "remoteaddr",
				Value:    "10.10.10.10",
				Required: true,
				Usage:    "remote address",
			},
			&cli.IntFlag{Name: "remoteport", Value: 27017},
			&cli.StringFlag{Name: "FQDN", Value: "www.baidu.com", Required: true},
			&cli.IntFlag{Name: "interval", Value: 1, Usage: "check interval,only for deamon."},
			&cli.IntFlag{Name: "failure", Value: 1, Usage: "check failure number,only for deamon."},
			&cli.Int64Flag{Name: "timeout", Value: 3},
			&cli.BoolFlag{Name: "deamon"},
		},
		Action: func(c *cli.Context) error {
			done := make(chan bool, 1)
			sigs := make(chan os.Signal)
			signal.Notify(sigs, syscall.SIGHUP, syscall.SIGUSR1)
			go func(c *cli.Context, done chan bool) {
				defer func() {
					if err := recover(); err != nil {
						fmt.Println("捕获异常:", err)
					}
				}()
				d := time.Duration(time.Second * time.Duration(c.Int("interval")))

				t := time.NewTicker(d)
				defer t.Stop()

				for {
					<-t.C
					if result, err := tcpGather(c.String("remoteaddr"), c.Int("remoteport"), c.Int64("timeout"), c.Int("failure")); err != nil {
						ChangeHost(c.String("FQDN"), c.String("localaddr"), c.String("remoteaddr"), result)
					} else {
						ChangeHost(c.String("FQDN"), c.String("localaddr"), c.String("remoteaddr"), result)
					}
					if !c.Bool("deamon") {
						done <- true
					}
				}
			}(c, done)
			go func() {
				<-sigs
				done <- true
			}()
			<-done
			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
