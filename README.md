# 简介
这是一个不断监测远程端口来修改本地hosts文件的小玩具

# 帮助
NAME:
   mcp - Monitor port to modify hosts

USAGE:
   mcp [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --localaddr value   local address (default: 127.0.0.1)
   --remoteaddr value  remote address (default: "10.10.10.10")
   --remoteport value  (default: 27017)
   --FQDN value        (default: "www.baidu.com")
   --interval value    check interval,only for deamon. (default: 1)
   --failure value     check failure number,only for deamon. (default: 1)
   --timeout value     (default: 3)
   --deamon            (default: false)
   --help, -h          show help (default: false)